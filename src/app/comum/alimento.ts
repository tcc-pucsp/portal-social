import { TabelaNutricional } from '../alimentos/tabela-nutricional';

export interface Alimento {

    nome: string;

    codigo: number;

    tabelaNutricional: TabelaNutricional;
    
    isCollapsed: boolean;
    
    caminhoImagem: string;

    quantidade: number;

}
