import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ComumModule } from './comum/comum.module';
import { SocialModule } from './servicos/social.module';
import { PrincipalComponent } from './principal/principal.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DisponivelComponent } from './alimentos/disponivel/disponivel.component';
import { InterceptorService } from './servicos/interceptor.service';
import { AutenticacaoGuardaService } from './servicos/autenticacao-guarda.service';
import { LanceModule } from './lance/lance.module';

@NgModule({
  declarations: [
    AppComponent,
    PrincipalComponent,
    DisponivelComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ComumModule,
    SocialModule,
    BrowserAnimationsModule,
    HttpClientModule,
    NgbModule,
    LanceModule
  ],
  providers: [{ provide: HTTP_INTERCEPTORS, useClass: InterceptorService, multi: true }, AutenticacaoGuardaService],
  bootstrap: [AppComponent]
})
export class AppModule { }
