export interface TabelaNutricional {

    umidade: string;
    energia: string;
    proteina: string;
    lipideos: string;
    colesterol: string;
    carboidratos: string;
    fibraAlimentar: string;
    calcio: string;
    magnesio: string;
    fosforo: string;
    ferro: string;
    sodio: string;
    potassio: string;
    cobre: string;
    zinco: string

}
