import { Component, OnInit } from '@angular/core';
import { AlimentosService as AlimentosService } from '../../servicos/alimentos.service';
import { Alimento } from '../../comum/alimento';
import { LancesService } from 'src/app/servicos/lances.service';

@Component({
  selector: 'app-disponivel',
  templateUrl: './disponivel.component.html',
  styleUrls: ['./disponivel.component.css']
})
export class DisponivelComponent implements OnInit {

  public alimentos: Alimento[];

  constructor(private alimentosService: AlimentosService, private lancesService: LancesService) {

    this.alimentosService.obterTodosAlimentos((alimentos => {
      this.alimentos = alimentos;
    }));

  }

  ngOnInit() {
  }

  public adicionarAlimento(alimento: Alimento) : void {
    this.lancesService.adicionarAlimento(alimento);
  }

  
}
