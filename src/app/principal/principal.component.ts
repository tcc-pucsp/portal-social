import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-principal',
  templateUrl: './principal.component.html',
  styleUrls: ['./principal.component.css']
})
export class PrincipalComponent implements OnInit {

  seta = '../../assets/images/double_arrow-24px.svg';
  doador = 'Seja um doador';
  necessitado = 'Precisa de doações?';
  empresarial = '../../assets/images/empresarial.png';
  social = '../../assets/images/social.png';

  constructor() { }

  ngOnInit() {
  }

}
