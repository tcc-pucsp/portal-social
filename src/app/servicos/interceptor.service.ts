import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { UsuarioService } from './usuario.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class InterceptorService implements HttpInterceptor {

  constructor(private usuarioService: UsuarioService, private router: Router) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    
    const token = this.usuarioService.obterToken();

    let modified = request;

    if (token !== null && request.headers.get("Authorization") === null) {

      modified = request.clone({
        setHeaders: { "Authorization": 'Bearer ' + token.access_token }
      });

    }

    return next.handle(modified).pipe(
      tap(() => {
      }, () => {
        this.router.navigate(["login-social"]);
      })
    )

  }

}
