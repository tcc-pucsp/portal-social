import { Injectable } from '@angular/core';


import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Login } from '../social/modelo/login';
import { Token } from '../social/modelo/token';


@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  constructor(private http: HttpClient) { }

  public realizarLogin(login: Login): Observable<Token> {

    const httpOptions = {

      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Basic ZXhlbXBsbzoxMjM='
      })

    };

    let body: HttpParams = new HttpParams();

    body = body
      .append('grant_type', 'password')
      .append('username', login.username)
      .append('password', login.password);

    return this
      .http
      .post<Token>("http://localhost:8083/oauth/token", body.toString(), httpOptions);

  }

  public autenticado(): Observable<any> {

    let token : Token = this.obterToken();

    if(token === null) {
      token = { access_token : "", token_type : ""};
    }
    
    const httpOptions = {

      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Basic ZXhlbXBsbzoxMjM='
      })

    };

    return this.http.get("http://localhost:8083/oauth/check_token?token=" + token.access_token, httpOptions);

  }

  public get logado() : boolean {
    return this.obterToken() !== null;
  }
  
  public salvarToken(token: Token): void {
    sessionStorage.setItem("token", JSON.stringify(token));
  }

  public obterToken(): Token {
    return JSON.parse(sessionStorage.getItem("token"));
  }

  public limparToken(): boolean {
    let autenticado = this.obterToken() !== null;
    sessionStorage.removeItem("token");
    return autenticado;
  }

  public salvarCnpj(cnpj: string): void {
    sessionStorage.setItem("cnpj", cnpj);
  }

  public obterCnpj(): string {
    return sessionStorage.getItem("cnpj");
  }

}
