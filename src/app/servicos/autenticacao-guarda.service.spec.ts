import { TestBed } from '@angular/core/testing';

import { AutenticacaoGuardaService } from './autenticacao-guarda.service';

describe('AutenticacaoGuardaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AutenticacaoGuardaService = TestBed.get(AutenticacaoGuardaService);
    expect(service).toBeTruthy();
  });
});
