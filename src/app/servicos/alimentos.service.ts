import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Alimento } from '../comum/alimento';

@Injectable({
  providedIn: 'root'
})
export class AlimentosService {

  constructor(private http: HttpClient) { }

  public obterTodosAlimentos(callback: (alimentos: Alimento[]) => void): void {

    const alimentos = this.obterAlimentos();

    if (alimentos === null) {

      this
        .http
        .get<Alimento[]>("http://localhost:8083/alimentos")
        .subscribe(alimentos => {

          this.salvarAlimentos(alimentos);

          callback(alimentos);

        });

    }

    callback(alimentos);

  }

  private salvarAlimentos(alimentos: Alimento[]): void {

    alimentos.forEach(alimento => {
      alimento.isCollapsed = true;
      alimento.caminhoImagem = "assets/images/alimentos/" + alimento.codigo + ".jpg";
      alimento.quantidade = 0;
    });

    sessionStorage.setItem("alimentos", JSON.stringify(alimentos));

  }

  private obterAlimentos(): Alimento[] {
    return JSON.parse(sessionStorage.getItem("alimentos"));
  }
  
}
