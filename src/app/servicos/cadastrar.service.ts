import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CadastrarService {

  constructor(private http: HttpClient) { }

  public cadastrarSocial(formulario: any): Observable<any> {

    console.log(formulario);


    const httpOptions = {

      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })

    };

    return this.http.post<any>("http://localhost:8083/social", formulario, httpOptions);

  }
}
