import { TestBed } from '@angular/core/testing';

import { LancesService } from './lances.service';

describe('LancesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LancesService = TestBed.get(LancesService);
    expect(service).toBeTruthy();
  });
});
