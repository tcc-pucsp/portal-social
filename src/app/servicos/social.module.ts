import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginSocialComponent } from '../social/login-social/login-social.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CadastrarSocialComponent } from '../social/cadastrar-social/cadastrar-social.component';
import { ComumModule } from '../comum/comum.module';

@NgModule({
  declarations: [LoginSocialComponent, CadastrarSocialComponent],
  imports: [
    CommonModule,
    ComumModule,
    ReactiveFormsModule
  ],
  exports: [LoginSocialComponent]
})
export class SocialModule { }
