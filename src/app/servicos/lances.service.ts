import { Injectable } from '@angular/core';
import { Alimento } from '../comum/alimento';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LancesService {

  _alimentos: Alimento[];

  constructor(private httpClient: HttpClient) {

    this._alimentos = [];

  }

  public publicarLance(): Observable<any> {

    return this.httpClient.post<any>("http://localhost:8083/publicar", this.alimentos);

  }

  public adicionarAlimento(alimento: Alimento): void {

    const alimentoPesquisado = this._alimentos.find(alimentoPesquisa => alimentoPesquisa.codigo === alimento.codigo);

    if (alimentoPesquisado == undefined) {

      this.aumentarQuantidade(alimento);

      this._alimentos.push(alimento);

    } else {

      this.aumentarQuantidade(alimentoPesquisado);

    }

  }

  public obterQuantidadeAlimentos(): number {

    return this.alimentos.length;

  }

  public get lanceVazio(): boolean {
    return this.obterQuantidadeAlimentos() === 0;
  }

  public limparLanceAlimentos(): void {

    this.alimentos.length = 0;

  }

  public aumentarQuantidade(alimento: Alimento): void {

    alimento.quantidade += 1;

  }

  diminuirQuantidade(alimento: Alimento) {

    const quantidade = alimento.quantidade - 1;

    if (quantidade > 0) {
      alimento.quantidade -= 1;
    } else {
      this.removerAlimento(alimento);
    }

  }

  public get alimentos(): Alimento[] {
    return this._alimentos;
  }

  public removerAlimento(alimento: Alimento): void {

    const index = this.alimentos.indexOf(alimento, 0);

    if (index > -1) {
      this.alimentos.splice(index, 1);
    }

  }

} 
