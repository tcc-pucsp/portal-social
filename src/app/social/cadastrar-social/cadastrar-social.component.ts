import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CadastrarService } from '../../servicos/cadastrar.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cadastrar-social',
  templateUrl: './cadastrar-social.component.html',
  styleUrls: ['./cadastrar-social.component.css']
})
export class CadastrarSocialComponent implements OnInit {

  @Input() error: string | null;

  @Output() submitEM = new EventEmitter();

  public form: FormGroup = new FormGroup({
    nomeInstituicao: new FormControl(''),
    telefone: new FormControl(''),
    email: new FormControl('', Validators.email),
    senha: new FormControl(''),
    endereco: new FormGroup({
      logradouro: new FormControl(''),
      numero: new FormControl(''),
      complemento: new FormControl(''),
      cep: new FormControl(''),
      cidade: new FormControl(''),
      estado: new FormControl('')
    }, Validators.required),
    cnpj: new FormGroup({
      registro: new FormControl('')
    })
  }, Validators.required);

  constructor(private cadastrarService: CadastrarService, private router: Router) { }

  ngOnInit() {
  }

  public cadastrar(): void {

    this
      .cadastrarService
      .cadastrarSocial(this.form.value)
      .toPromise()
      .then(response => this.router.navigate(["alimentos-disponiveis"]));

  }

}
