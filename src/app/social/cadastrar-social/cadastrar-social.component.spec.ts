import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadastrarSocialComponent } from './cadastrar-social.component';

describe('CadastrarSocialComponent', () => {
  let component: CadastrarSocialComponent;
  let fixture: ComponentFixture<CadastrarSocialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadastrarSocialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadastrarSocialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
