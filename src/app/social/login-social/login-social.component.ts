import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { UsuarioService } from 'src/app/servicos/usuario.service';

@Component({
  selector: 'app-login-social',
  templateUrl: './login-social.component.html',
  styleUrls: ['./login-social.component.css']
})
export class LoginSocialComponent implements OnInit {

  @Input() error: string | null;

  @Output() submitEM = new EventEmitter();

  public form: FormGroup = new FormGroup({
    username: new FormControl(''),
    password: new FormControl('')
  });

  constructor(private usuarioService: UsuarioService, private router: Router) { 
    this.usuarioService.limparToken();
  }

  ngOnInit() {
  }

  public login(): void {

    this
      .usuarioService
      .realizarLogin(this.form.value)
      .subscribe(token => {

        if(token){

          this.usuarioService.salvarToken(token);

          this.router.navigate(['/alimentos-disponiveis']);

        }
      });

  }

  submit() {
    if (this.form.valid) {
      this.submitEM.emit(this.form.value);
    }
  }


}
