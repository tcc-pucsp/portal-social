import { Component } from '@angular/core';
import { UsuarioService } from './servicos/usuario.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title = 'Filantro Flex';

  constructor(private usuarioService: UsuarioService, private router: Router) {}

  public logout() : void {

    let autenticado = this.usuarioService.limparToken();

    if(autenticado){
      this.router.navigate(["/"]);
    }

  }

  public get logado() : boolean {
    return this.usuarioService.logado;
  }
  
}
