import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginSocialComponent } from './social/login-social/login-social.component';
import { PrincipalComponent } from './principal/principal.component';
import { CadastrarSocialComponent } from './social/cadastrar-social/cadastrar-social.component';
import { DisponivelComponent as AlimentosDisponivelComponent } from './alimentos/disponivel/disponivel.component';
import {AutenticacaoGuardaService as AutenticacaoGuarda, AutenticacaoGuardaService} from './servicos/autenticacao-guarda.service';
import { InformacoesLanceComponent } from './lance/informacoes-lance/informacoes-lance.component';
import { LanceSucessoComponent } from './lance/lance-sucesso/lance-sucesso.component';

const routes: Routes = [
  { 'path': '', component: PrincipalComponent },
  { 'path': 'login-social', component: LoginSocialComponent },
  { 'path': 'cadastrar-social', component: CadastrarSocialComponent},
  { 'path': 'alimentos-disponiveis', component: AlimentosDisponivelComponent, canActivate: [AutenticacaoGuarda]},
  { 'path': 'lances', component: InformacoesLanceComponent, canActivate: [AutenticacaoGuarda]},
  { 'path': 'lances-concluido', component: LanceSucessoComponent, canActivate: [AutenticacaoGuarda]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AutenticacaoGuardaService]
})
export class AppRoutingModule { }
