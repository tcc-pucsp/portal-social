import { Component, OnInit } from '@angular/core';
import { Alimento } from 'src/app/comum/alimento';
import { LancesService } from 'src/app/servicos/lances.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-informacoes-lance',
  templateUrl: './informacoes-lance.component.html',
  styleUrls: ['./informacoes-lance.component.css']
})
export class InformacoesLanceComponent implements OnInit {

  public alimentos: Alimento[];

  constructor(private lancesServico: LancesService, private router: Router) {

    this.alimentos = this.lancesServico.alimentos;

  }

  ngOnInit() {
    this.casoLanceVazioRedirecionar();
  }

  public adicionarUnidade(alimento: Alimento): void {
    this.lancesServico.aumentarQuantidade(alimento);
  }

  public diminuirUnidade(alimento: Alimento): void {
    this.lancesServico.diminuirQuantidade(alimento);
    this.casoLanceVazioRedirecionar();
  }

  public removerAlimento(alimento: Alimento): void {
    this.lancesServico.removerAlimento(alimento);
    this.casoLanceVazioRedirecionar();
  }

  public get lanceVazio(): boolean {
    return this.lancesServico.lanceVazio;
  }

  private casoLanceVazioRedirecionar(): void {
    if (this.lanceVazio) this.router.navigate(["alimentos-disponiveis"]);
  }

  public publicarLance(): void {

    this.lancesServico.publicarLance().subscribe(() => {
      this.router.navigate(["lances-concluido"]);
    });

  }
}
