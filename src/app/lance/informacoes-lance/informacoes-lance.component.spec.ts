import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InformacoesLanceComponent } from './informacoes-lance.component';

describe('InformacoesLanceComponent', () => {
  let component: InformacoesLanceComponent;
  let fixture: ComponentFixture<InformacoesLanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InformacoesLanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InformacoesLanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
