import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LancesAlimentoComponent } from './lances-alimento/lances-alimento.component';
import { InformacoesLanceComponent } from './informacoes-lance/informacoes-lance.component';
import { LanceSucessoComponent } from './lance-sucesso/lance-sucesso.component';



@NgModule({
  declarations: [LancesAlimentoComponent, InformacoesLanceComponent, LanceSucessoComponent],
  imports: [
    CommonModule
  ],
  exports: [LancesAlimentoComponent]
})
export class LanceModule { }
