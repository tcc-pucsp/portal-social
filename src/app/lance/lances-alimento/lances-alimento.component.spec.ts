import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LancesAlimentoComponent } from './lances-alimento.component';

describe('LancesAlimentoComponent', () => {
  let component: LancesAlimentoComponent;
  let fixture: ComponentFixture<LancesAlimentoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LancesAlimentoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LancesAlimentoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
