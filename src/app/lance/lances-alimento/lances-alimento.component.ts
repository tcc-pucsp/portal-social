import { Component, OnInit } from '@angular/core';
import { LancesService } from 'src/app/servicos/lances.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-lances-alimento',
  templateUrl: './lances-alimento.component.html',
  styleUrls: ['./lances-alimento.component.css']
})
export class LancesAlimentoComponent implements OnInit {

  constructor(private lancesService: LancesService, private router: Router) {
  }

  ngOnInit() {
  }

  public obterQuantidadeAlimentos(): string {

    const quantidadeAlimentos = this.lancesService.obterQuantidadeAlimentos();

    if (quantidadeAlimentos > 0) {
      return " + " + quantidadeAlimentos + " alimentos";
    }

    return "vazio";

  }

  public limparLanceAlimentos(): void {

    this.lancesService.limparLanceAlimentos();

  }

  public obterInformacoesLance(): void {

    if (this.lancesService.obterQuantidadeAlimentos() > 0) {
      this.router.navigate(["lances"]);
    }

  }

  public get lanceVazio() : boolean {
    return this.lancesService.lanceVazio;
  }

}
