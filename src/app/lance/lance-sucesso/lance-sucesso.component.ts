import { Component, OnInit } from '@angular/core';
import { LancesService } from 'src/app/servicos/lances.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-lance-sucesso',
  templateUrl: './lance-sucesso.component.html',
  styleUrls: ['./lance-sucesso.component.css']
})
export class LanceSucessoComponent implements OnInit {

  constructor(private lanceServico: LancesService, private router: Router) {

  }

  ngOnInit() {

    if (this.lanceServico.lanceVazio) this.router.navigate(["alimentos-disponiveis"]);
    else {
      this.lanceServico.limparLanceAlimentos();
    }
  }

}
