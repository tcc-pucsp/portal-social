import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LanceSucessoComponent } from './lance-sucesso.component';

describe('LanceSucessoComponent', () => {
  let component: LanceSucessoComponent;
  let fixture: ComponentFixture<LanceSucessoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LanceSucessoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LanceSucessoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
